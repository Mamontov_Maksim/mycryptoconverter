package com.mamontov.project.coincalkulator;

import android.annotation.SuppressLint;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.SearchView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class CoinCalculatorFragment extends Fragment {

    private RecyclerView mRecyclerView;
    private EditText mPryceEditText;
    private Button mUSDButton;
    private Button mUAHButton;

    private CoinAdapter mAdapter;

    private double mPrice;
    private String mConvert = "";

    private List<Coin> mCoins = new ArrayList<>();

    public static CoinCalculatorFragment init(){
        return new CoinCalculatorFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_coin_calculator, container, false);

        mRecyclerView = (RecyclerView) view.findViewById(R.id.recycler_crypto_coin);
        mRecyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 1));

        mPryceEditText = (EditText) view.findViewById(R.id.pryce_edit_text);
        mPryceEditText.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!s.toString().equals("")){
                   mPrice = new Double(s+"");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        mUSDButton = (Button) view.findViewById(R.id.USD_button);
        mUSDButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!mConvert.equals("USD")) {
                    mConvert = "USD";
                    new FetchItemsTask().execute();
                } else {
                    updateUI();
                }
            }
        });

        mUAHButton = (Button) view.findViewById(R.id.UAH_button);
        mUAHButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!mConvert.equals("UAH")) {
                    mConvert = "UAH";
                    new FetchItemsTask().execute();
                } else {
                    updateUI();
                }
            }
        });


        return view;
    }

    /**
     *
     */
    private class CoinHolder extends RecyclerView.ViewHolder{

        private Button mCoinNameButton;
        private TextView mPryceInCoinText;
        private EditText mNameCoinEditText;
        private CheckBox mCheckBox;

        private Coin mCoinHolder;


        /**
         *
         * @param layoutInflater
         * @param itemGroup
         */
        public CoinHolder(LayoutInflater layoutInflater,@NonNull ViewGroup itemGroup) {
            super(layoutInflater.inflate(R.layout.list_irem_coin_crypto, itemGroup, false));
            mCoinNameButton = (Button) itemView.findViewById(R.id.name_crypto_coin_button);
            mPryceInCoinText = itemView.findViewById(R.id.pryce_text);
            mNameCoinEditText = itemView.findViewById(R.id.name_crypto_coin_edit_text);
            mCheckBox = itemView.findViewById(R.id.favorite_check_box);
            mCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    mCoinHolder.setLike(isChecked);
                    if (isChecked){
                        mCheckBox.setButtonDrawable(R.mipmap.ic_favorite);
                    } else {
                        mCheckBox.setButtonDrawable(R.mipmap.ic_favorite_border);
                    }
                }
            });
        }

        /**
         *
         * @param coin
         */
        public void bind(Coin coin){
            mCoinHolder = coin;
            mCoinNameButton.setText(mCoinHolder.getSymbol());
            mNameCoinEditText.setText(mPrice + "  " + mCoinHolder.getName() + " =");
            mPryceInCoinText.setText(String.format("%.4f", mCoinHolder.getCount() * mPrice) + " " + mConvert);
            mCheckBox.setChecked(mCoinHolder.isLike());

        }
    }

    /**
     *
     */
    private class CoinAdapter extends RecyclerView.Adapter<CoinHolder>{

        private List<Coin> mCoinsAdapter;

        CoinAdapter(List<Coin> coins){
            filterAdapter(coins);
        }


        @NonNull
        @Override
        public CoinHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
            return new CoinHolder(layoutInflater, viewGroup);
        }

        @Override
        public void onBindViewHolder(@NonNull CoinHolder coinHolder, int i) {
            Coin coin = mCoinsAdapter.get(i);
            coinHolder.bind(coin);
        }

        @Override
        public int getItemCount() {
            return mCoinsAdapter.size();
        }

        /**
         *
         * @param text
         */
        public void filterAdapter(String text){
            List<Coin> coinList = new ArrayList<>();

            if (text.equals("")){
                mCoinsAdapter = mCoins;
                return;
            }

            //TODO (!ALGORITHMS SEARCH implementation)
            for (Coin coin : mCoins){
                if (text.charAt(0) == coin.getName().charAt(0)){
                    coinList.add(coin);
                }
            }

            mCoinsAdapter = coinList;
        }

        public void filterAdapter(List<Coin> coins){
            List<Coin> coinFirst = new ArrayList<>();
            List<Coin> coinLast = new ArrayList<>();

            for (Coin coin : coins){
                if (coin.isLike()){
                    coinFirst.add(coin);
                } else {
                    coinLast.add(coin);
                }
            }
            mCoinsAdapter = coinFirst;
            mCoinsAdapter.addAll(coinLast);
        }
    }

    /**
     *
     */
    private class FetchItemsTask extends AsyncTask<Void,Void,List<Coin>> {

        @Override
        protected List<Coin> doInBackground(Void... voids) {
            return new CoinMarket().fetchItems(mConvert);
        }

        @Override
        protected void onPostExecute(List<Coin> items) {
            mCoins = items;
            updateUI();
        }
    }

    /**
     * Обновляет рециклер
     */
    private void updateUI(){
        if(isAdded()) {
            mAdapter = new CoinAdapter(mCoins);
            mRecyclerView.setAdapter(mAdapter);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.main_menu, menu);

        MenuItem menuItem = menu.findItem(R.id.app_bar_search);

        SearchView searchView  = (SearchView) menuItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return true;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                if (s != null) {
                    mAdapter.filterAdapter(s);
                    //Обновляет лист в адаптере
                    mAdapter.notifyDataSetChanged();
                    return true;
                }
                return true;
            }
        });

        super.onCreateOptionsMenu(menu, inflater);
    }

}
