package com.mamontov.project.coincalkulator.database

class Schema {
    class TABLE {
        val TABLE_NAME: String = "FavoriteCryptoCoin"
        class COLS {
            val ID_COIN: String = "uuid"
            val COIN_NAME: String = "coinName"
            val COIN_SYMBOL: String = "coinSymbol"
            val COIN_PRYCE_UAH: String = "coinPriceUAH"
            val COIN_PRICE_USD: String = "coinPriceUSD"
        }
    }
}