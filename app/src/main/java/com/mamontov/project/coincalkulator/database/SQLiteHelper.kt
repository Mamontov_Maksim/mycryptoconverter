package com.mamontov.project.coincalkulator.database

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper


class SQLiteHelper(context: Context):
        SQLiteOpenHelper(context, "MyDataBase", null, 1){

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        TODO("ПОКА ЧТО НЕ ТРОГАЕМ")
    }

    override fun onCreate(db: SQLiteDatabase) {
        db.execSQL("CREATE TABLE ${Schema.TABLE().TABLE_NAME} (_id INTEGER PRIMARY KEY AUTOINCREMENT," +
                " ${Schema.TABLE.COLS().ID_COIN}, " +
                " ${Schema.TABLE.COLS().COIN_NAME}" +
                " ${Schema.TABLE.COLS().COIN_SYMBOL}," +
                " ${Schema.TABLE.COLS().COIN_PRYCE_UAH}," +
                " ${Schema.TABLE.COLS().COIN_PRICE_USD})")
    }

}
