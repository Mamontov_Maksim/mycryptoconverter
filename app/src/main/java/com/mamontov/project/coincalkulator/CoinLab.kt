package com.mamontov.project.coincalkulator

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import com.mamontov.project.coincalkulator.database.CoinCursorWrapper
import com.mamontov.project.coincalkulator.database.SQLiteHelper
import java.util.*

//Сиглтон в котлине
class CoinLab private constructor(context: Context){

    private var mDBase: SQLiteDatabase

    init {
        val mContext: Context = context.applicationContext
        mDBase = SQLiteHelper(mContext).writableDatabase
    }

    companion object {
        var coinLab: CoinLab? = null

        fun getInitstate(context: Context): CoinLab{
            if (coinLab == null){
                coinLab = CoinLab(context)
            }
            return coinLab!!
        }
    }

    fun getCoin(uuid: UUID): Coin{
        TODO("RETURN ONE COIN")
    }

    fun getCoins(): List<Coin>{
        TODO("RETURN ALL!! COINS")
    }

    fun addCoin(){
        TODO("ADD COIN IN MY TABLE")
    }

    fun updateCoin(){
        TODO("UPDATE COIN IN DATA BASE")
    }

    fun deletedCoin(){
        TODO("DELETED COIN IN MY TABLE")
    }

    fun getContentValues(): ContentValues{
        TODO("PUT DATA IN CONTENT VALUES")
    }

    fun queryCoin(): CoinCursorWrapper{
        TODO("CREATE CURSOR AND CREATE REQUEST IN MY TABLE))")
    }

}