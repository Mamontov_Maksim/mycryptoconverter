package com.mamontov.project.coincalkulator;

public class Coin {

    private String mName;
    private String mSymbol;
    private double mCount;
    private boolean mIsLike = false;

    public double getCount() {
        return mCount;
    }

    public void setCount(double mCount) {
        this.mCount = mCount;
    }

    public String getSymbol() {
        return mSymbol;
    }

    public void setSymbol(String mSymbol) {
        this.mSymbol = mSymbol;
    }

    public String getName() {
        return mName;
    }

    public void setName(String mName) {
        this.mName = mName;
    }

    public boolean isLike() {
        return mIsLike;
    }

    public void setLike(boolean like) {
       mIsLike = like;
    }
}
