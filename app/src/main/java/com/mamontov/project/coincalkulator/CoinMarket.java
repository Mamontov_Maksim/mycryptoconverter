package com.mamontov.project.coincalkulator;

import android.net.Uri;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class CoinMarket {

    private static final String API_KEY = "627a32ca-5c66-454c-9f05-cd057e4b05e4";

    private String mConvert = "";
    private List<Coin> items = new ArrayList<>();

    //Читает сайт и вывод его в массив байтов
    public byte[] getUrlBytes(String urlSpec) throws IOException {
        URL url = new URL(urlSpec);
        HttpURLConnection connection = (HttpURLConnection)url.openConnection();
        try {
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            InputStream in = connection.getInputStream();
            if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                throw new IOException(connection.getResponseMessage() +
                        ": with " +
                        urlSpec);
            }
            int bytesRead = 0;
            byte[] buffer = new byte[1024];
            while ((bytesRead = in.read(buffer)) > 0) {
                out.write(buffer, 0, bytesRead);
            }
            out.close();
            return out.toByteArray();
        } finally {
            connection.disconnect();
        }
    }

    public String getUrlString(String urlSpec) throws IOException {
        return new String(getUrlBytes(urlSpec));
    }

    //Читает сайт и переводит его в json
    public List<Coin> fetchItems(String convert) {
        mConvert = convert;
        try {
            String url = Uri.parse("https://pro-api.coinmarketcap.com/v1/cryptocurrency/listings/latest")
                    .buildUpon()
                    .appendQueryParameter("CMC_PRO_API_KEY", API_KEY)
                    .appendQueryParameter("start", "1")
                    .appendQueryParameter("limit", "100")
                    .appendQueryParameter("convert", mConvert)
                    .build().toString();
            String jsonString = getUrlString(url);
            getCoins(items, new JSONObject(jsonString));
        } catch (IOException eoi) {
            eoi.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return items;
    }

    //Получаем список
    private void getCoins(List<Coin> items, JSONObject json) throws JSONException {
        JSONArray jsonArray = json.optJSONArray("data");

        for (int i = 0; i < jsonArray.length(); i++){
            JSONObject firstJsonObject = jsonArray.getJSONObject(i);
            Coin coin = new Coin();
            coin.setSymbol(firstJsonObject.getString("symbol"));
            coin.setName(firstJsonObject.getString("name"));
            JSONObject secondJsonObject = firstJsonObject.getJSONObject("quote");
            JSONObject threeJsonObject = secondJsonObject.getJSONObject(mConvert);
            coin.setCount(threeJsonObject.getDouble("price"));
            if(i%2 == 0){
                coin.setLike(true);
            }
            items.add(coin);
        }
    }
}
