package com.mamontov.project.coincalkulator;

import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import org.json.JSONException;

import java.util.List;

public class CoinCalculatorActivity extends SingleActivityFragment {

    @Override
    protected Fragment creatFragment() {
        return CoinCalculatorFragment.init();
    }
}
